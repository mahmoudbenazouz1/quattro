<?php

namespace App\Http\Controllers;

use App\Http\Requests\BulkRequest;
use App\Notifications\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\PhpWord;
use Symfony\Component\Mime\Part\TextPart;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Laravel\Socialite\Facades\Socialite;




class BulkController extends Controller
{
    public function index()
    {
        return view('index');
    }
    public function request(BulkRequest $request)
    {
        $uploadedFile = $request->file('file');
        $extension = $uploadedFile->getClientOriginalExtension();

        $validator = Validator::make([], []);
        if ($extension !== 'csv') {
            $validator->errors()->add('file', __('boilerplate::forms.request.csv_file'));
            return redirect()->back()->withErrors($validator);
        }

        $headers = ['names', 'emails'];
        $path = $request->file('file')->getRealPath();
        $headerBol = array_diff(fgetcsv(fopen($path, 'r')), $headers);

        if ($headerBol != []) {
            $validator->errors()->add('file', __('boilerplate::forms.request.header_error'));
            return redirect()->back()->withErrors($validator);
        }

        $wordFile = $request->file('wordFile');

        $phpWord = new PhpWord();

        // Load the Word file
        $phpWord = IOFactory::load($wordFile->getPathname());

        // Save the Word file as HTML
        $htmlFilePath = storage_path('app/public/') . $wordFile->getClientOriginalName() . '.html';
        $phpWord->save($htmlFilePath, 'HTML');

        // Read the HTML content from the file
        $htmlContent = file_get_contents($htmlFilePath);

        $file = file($path);
        $rowsCount = count($file);

        if ($rowsCount == 1) {
            $validator->errors()->add('file', __('boilerplate::forms.request.one_row'));
            return redirect()->back()->withErrors($validator);
        }

        $csvFile = fopen($path, 'r');
        $combinedData = [];
        $combinedRules = [];
        $firstline = true;
        $rowNumber = 2; // Initialize row count
        while (($row = fgetcsv($csvFile)) !== false) {
            if (!$firstline) {
                $name = $row[0];
                $email = $row[1];
                $additionalData = [
                    'name_in_line_' . $rowNumber => $name,
                    'email_in_line_' . $rowNumber => $email,
                ];
                $additionalRules = [
                    'name_in_line_' . $rowNumber => 'required',
                    'email_in_line_' . $rowNumber => 'required|email',
                ];
                $combinedData = array_merge($combinedData, $additionalData);
                $combinedRules = array_merge($combinedRules, $additionalRules);
                $rowNumber++; // Increment row count
            }
            $firstline = false;
        }
        fclose($csvFile);
        $validator = Validator::make($combinedData, $combinedRules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            foreach ($request->file('files') as $file) {
                $path_file = $file->storeAs('temporary_folder', $file->getClientOriginalName());
                $filePaths[] = $path_file;
            }
            $csvFile = fopen($path, 'r');
            $firstline = true;
            while (($row = fgetcsv($csvFile)) !== false) {
                if (!$firstline) {
                    $name = strtok($row[0], " ");;
                    $email = $row[1];
                    $htmlContent = str_replace('[first name]', $name, $htmlContent);
                    Mail::send([], [], function ($message) use ($htmlContent, $email, $request, $filePaths) {
                        $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_ADDRESS_NAME'))
                            ->to($email)
                            ->subject($request->subject)
                            ->html($htmlContent);

                        foreach ($filePaths as $filePath) {
                            $message->attach(storage_path('app/' . $filePath));
                        }
                    });
                    $htmlContent = str_replace($name, '[first name]', $htmlContent);
                }
                $firstline = false;
            }
            fclose($csvFile);
            File::delete($htmlFilePath);
            Storage::delete($filePaths);

            return redirect()->route('boilerplate.request.index')
                ->with('growl', [__('boilerplate::forms.request.bulk_done', [
                    'rowsCount' => $rowsCount - 1,
                ]), 'success']);
        }
    }

    public function redirectToMicrosoft()
    {
        return Socialite::driver('microsoft')->redirect();
    }

    public function handleMicrosoftCallback(Request $request)
    {
        $user = Socialite::driver('microsoft')->user();

        // Retrieve the access token
        $accessToken = $user->token;
        dd($accessToken);

        // Use $accessToken for making requests to Microsoft Graph API
        $response = $this->sendEmailViaGraphAPI($accessToken);

        // Handle the response accordingly

        return "Email sent successfully!";
    }

    private function sendEmailViaGraphAPI($accessToken)
    {
        // Prepare email data
        $emailData = [
            'message' => [
                'subject' => 'Your email subject',
                'body' => [
                    'content' => 'Your email content',
                    'contentType' => 'HTML',
                ],
                'toRecipients' => [
                    ['emailAddress' => ['address' => 'mahmoud.ben.azouz@gmail.com']],
                ],
            ],
        ];

        // Send email using Outlook API
        $response = Http::withToken($accessToken)
            ->post('https://graph.microsoft.com/v1.0/me/sendMail', $emailData);

        return $response;
    }
}
