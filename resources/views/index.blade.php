@extends('boilerplate::forms.request', ['title' => __('boilerplate::forms.request.title')])
<section class="content">
    <div class="container-fluid">

        @component('boilerplate::form', ['route' => 'boilerplate.request.request', 'enctype'=>'multipart/form-data'])
        <div class="row">
            <div class="col-lg-12">
                @component('boilerplate::card', ['title' => 'boilerplate::forms.request.title_form'])
                <!-- <div class="login-logo">
                    <img src="{{url('/logo.jpg')}}" width="500" height="300">
                </div> -->
                <div class="row">
                    <div class="col-lg-6">
                        @component('boilerplate::input', ['name' => 'subject', 'label' => 'boilerplate::forms.request.subject', 'type' => 'text'])@endcomponent
                        <p>Emails</p>
                        <input name="file" type="file"><br>
                        @error('file')
                        <div class="error-bubble">
                            <div>{{$message}}</div>
                        </div>
                        @enderror
                        <p>Word</p>
                        <input name="wordFile" type="file">
                        @error('wordFile')
                        <div class="error-bubble">
                            <div>{{$message}}</div>
                        </div>
                        @enderror
                    </div>
                    <div class="col-lg-6">
                        <img src="{{ asset('logo.png') }}" alt="Logo" style="float: right; margin-left: 10px;">
                    </div>

                </div>
                <br>
                <p><button class="btn btn-info" type="button" onclick="addInputFile()">Add Input File</button></p>
                <div id="fileContainer">
                </div>
                @endcomponent
                <p align="right">
                    <button type="submit" class="btn btn-dark">@lang('boilerplate::forms.request.submit')</button>
                </p>
            </div>
        </div>
        @endcomponent
    </div>
</section>

<script>
    function addInputFile() {
        // Get the container where input file elements are stored
        var fileContainer = document.getElementById('fileContainer');

        // Create a new input file element
        var newInputFile = document.createElement('input');
        newInputFile.type = 'file';
        newInputFile.name = 'files[]'; // You can adjust the name attribute as needed

        // Generate a unique ID for the new input file element
        var fileId = 'files' + (fileContainer.children.length + 1);
        newInputFile.id = fileId;

        // Append the new input file element to the container
        fileContainer.appendChild(newInputFile);
    }
</script>