<?php

return [
    'request' => [
        'title'                  => 'Quattro Sender',
        'title_form'                  => 'Quattro Sender',
        'subject'                  => 'Subject',
        'submit'                  => 'Submit',
        'csv_file'         => 'The file must be a file of type: csv.',
        'header_error'         => 'Invalid column headers. Please refer attached template.',
        'one_row'         => 'The file must have at least one line.',
        'bulk_done'         => 'Execution completed. :rowsCount emails have been sent.',

    ],
];
