<?php

use App\Http\Controllers\BulkController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::group([
    // 'prefix'     => config('boilerplate.app.prefix', ''),
    'domain'     => config('boilerplate.app.domain', ''),
    'middleware' => ['web', 'boilerplate.locale'],
    'as'         => 'boilerplate.',
], function () {
    Route::get('/', [BulkController::class, 'index'])->name('request.index');
    Route::post('/request', [BulkController::class, 'request'])->name('request.request');


    Route::get('/microsoft/authorize', [BulkController::class, 'redirectToMicrosoft']);
    Route::get('/microsoft/callback', [BulkController::class, 'handleMicrosoftCallback']);
});
